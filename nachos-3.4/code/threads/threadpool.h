#ifndef THREADPOOL_H
#define THREADPOOL_H

#include "copyright.h"
#include "thread.h"
#include <queue>
#include <vector>

const int MAX_THREADS = 128;

class ThreadPool
{
private:
  int max_threads;
  std::queue<int> avail_tids;
  std::vector<Thread *> threads;
  int acquireTId();

public:
  ThreadPool(int m = MAX_THREADS);

  Thread *NewThread(char *debugName, int userid = 0, int priority = 0);
  void releaseTId(int tId);
  void PrintThreads();
};
#endif