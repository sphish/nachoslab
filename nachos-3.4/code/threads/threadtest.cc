// threadtest.cc
//	Simple test case for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield,
//	to illustratethe inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "elevatortest.h"

// testnum is set in main.cc
int testnum = 1;

//----------------------------------------------------------------------
// SimpleThread
// 	Loop 5 times, yielding the CPU to another ready thread
//	each iteration.
//
//	"which" is simply a number identifying the thread, for debugging
//	purposes.
//----------------------------------------------------------------------

void SimpleThread(int which)
{
    int num;

    for (num = 0; num < 5; num++)
    {
        printf("*** thread %d looped %d times\n, ", which, num);
        currentThread->Yield();
    }
}

void PrintTId(int times)
{
    for (int i = 0; i < times; i++)
    {
        printf("*** Thread(tId=%d, userId=%d, priority=%d, name=%s) looped %d times.\n", currentThread->getTId(), currentThread->getUserId(), currentThread->getPriority(), currentThread->getName(), i);
        currentThread->Yield();
    }
    printf("*** Thread(tId=%d, userId=%d, priority=%d, name=%s) finished.\n", currentThread->getTId(), currentThread->getUserId(), currentThread->getPriority(), currentThread->getName());
}
//----------------------------------------------------------------------
// ThreadTest1
// 	Set up a ping-pong between two threads, by forking a thread
//	to call SimpleThread, and then calling SimpleThread ourselves.
//----------------------------------------------------------------------

void ThreadTest1()
{
    DEBUG('t', "Entering ThreadTest1\n");

    Thread *t = Thread::NewThread("forked thread");

    t->Fork(SimpleThread, (void *)1);
    SimpleThread(0);
}

void ThreadTest2()
{
    DEBUG('t', "Entering ThreadTest2\n");

    for (int i = 0; i < 2; i++)
    {
        char thread_name[15];
        sprintf(thread_name, "print tId%d", i);
        Thread *t = Thread::NewThread(thread_name);
        t->Fork(PrintTId, (void *)(i * 2 + 1));
    }
    for (int i = 0; i < 5; i++)
    {
        threadpool->PrintThreads();
        currentThread->Yield();
    }
}

void ThreadTest3()
{
    DEBUG('t', "Entering ThreadTest3\n");

    for (int i = 0; i < 129; i++)
    {
        char *thread_name = new char[15];
        sprintf(thread_name, "print tId%d", i);
        Thread *t = Thread::NewThread(thread_name);
        if (t == nullptr)
        {
            printf("Failed to create thread %s.\n", thread_name);
            continue;
        }
        t->Fork(PrintTId, (void *)(i & 3));
    }
    currentThread->Yield();
    Thread *t = Thread::NewThread("test");
    printf("The fisrt released tId is %d\n", t->getTId());
}

void ThreadTest4()
{
    DEBUG('t', "Entering ThreadTest4\n");
    for (int i = 0; i < 4; i++)
    {
        char *thread_name = new char[15];
        sprintf(thread_name, "print tId%d", i);
        Thread *t = Thread::NewThread(thread_name, 0, i);
        if (t == nullptr)
        {
            printf("Failed to create thread %s.\n", thread_name);
            continue;
        }
        t->Fork(PrintTId, (void *)3);
    }
}
//----------------------------------------------------------------------
// ThreadTest
// 	Invoke a test routine.
//----------------------------------------------------------------------

void ThreadTest()
{
    switch (testnum)
    {
    case 4:
        ThreadTest4();
        break;
    case 3:
        ThreadTest3();
        break;
    case 2:
        ThreadTest2();
        break;
    case 1:
        ThreadTest1();
        break;
    default:
        printf("No test specified.\n");
        break;
    }
}
