#include "threadpool.h"
#include "system.h"

ThreadPool::ThreadPool(int m) : max_threads(m)
{
  for (int i = 0; i < m; i++)
    avail_tids.push(i);
  threads.resize(m);
}

int ThreadPool::acquireTId()
{
  IntStatus oldLevel = interrupt->SetLevel(IntOff);
  if (avail_tids.empty())
    return -1;
  int result = avail_tids.front();
  avail_tids.pop();
  (void)interrupt->SetLevel(oldLevel);
  return result;
}

void ThreadPool::releaseTId(int tId)
{
  IntStatus oldLevel = interrupt->SetLevel(IntOff);
  avail_tids.push(tId);
  threads[tId] = nullptr;
  (void)interrupt->SetLevel(oldLevel);
}

Thread *ThreadPool::NewThread(char *threadName, int userid, int priority)
{
  int tid = acquireTId();
  if (tid == -1)
  {
    DEBUG('f', "Failed to aquire TId.\n");
    return NULL;
  }
  return threads[tid] = new Thread(threadName, tid, userid, priority);
}

void ThreadPool::PrintThreads()
{
  printf("TID USER STATUS NAME\n");
  for (Thread *t : threads)
    if (t != nullptr)
      printf("%3d %4d %d      %s\n", t->tId, t->userId, t->status, t->name);
}